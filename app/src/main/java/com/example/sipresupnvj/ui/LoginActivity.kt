package com.example.sipresupnvj.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.sipresupnvj.databinding.ActivityLoginBinding
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initiateUI()
    }

    private fun initiateUI() {
        binding.run {
            btnLogin.setOnClickListener {
                login()
            }

            tvRegister.setOnClickListener {
                startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
            }
        }
    }

    private fun login() {
        val email = binding.email.text.toString().trim()
        val password = binding.password.text.toString().trim()

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(applicationContext, "Enter email address!", Toast.LENGTH_SHORT).show()
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(applicationContext, "Enter password!", Toast.LENGTH_SHORT).show()
        }

        if (password.length < 6) {
            Toast.makeText(
                applicationContext,
                "Password too short, enter minimum 6 characters!",
                Toast.LENGTH_SHORT
            ).show()
        }

        val auth = FirebaseAuth.getInstance()
        binding.progressBar.visibility = View.VISIBLE
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this@LoginActivity) { task ->
                binding.progressBar.visibility = View.GONE
                if (!task.isSuccessful) {
                    Toast.makeText(
                        this@LoginActivity, "Authentication failed." + task.exception,
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("LoginActivity", "Authentication failed." + task.exception)
                } else {
                    Log.d("LoginActivity", "signInWithEmail:success")
                    startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                    finish()
                }
            }
    }
}