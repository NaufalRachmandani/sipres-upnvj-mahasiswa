package com.example.sipresupnvj.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.sipresupnvj.databinding.ActivityRegisterBinding
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth


class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initiateUI()
    }

    private fun initiateUI() {
        binding.run {
            btnRegister.setOnClickListener {
                register()
            }

            ivBack.setOnClickListener {
                onBackPressed()
            }
        }
    }

    private fun register() {
        val email = binding.email.text.toString().trim()
        val password = binding.password.text.toString().trim()

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(applicationContext, "Enter email address!", Toast.LENGTH_SHORT).show()
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(applicationContext, "Enter password!", Toast.LENGTH_SHORT).show()
        }

        if (password.length < 6) {
            Toast.makeText(
                applicationContext,
                "Password too short, enter minimum 6 characters!",
                Toast.LENGTH_SHORT
            ).show()
        }

        val auth = FirebaseAuth.getInstance()
        binding.progressBar.visibility = View.VISIBLE
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this@RegisterActivity) { task ->
                binding.progressBar.visibility = View.GONE
                if (!task.isSuccessful) {
                    Toast.makeText(
                        this@RegisterActivity, "Authentication failed." + task.exception,
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("RegisterActivity", "Authentication failed." + task.exception)
                } else {
                    Log.d("RegisterActivity", "signInWithEmail:success")
                    startActivity(Intent(this@RegisterActivity, LoginActivity::class.java))
                    finish()
                }
            }
    }
}